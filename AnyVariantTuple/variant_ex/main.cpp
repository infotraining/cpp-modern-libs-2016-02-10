#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>
#include <type_traits>
#include <boost/variant.hpp>

using namespace std;

class AbsVisitor : public boost::static_visitor<double>
{
public:
    template <typename T>
    auto operator()(const T& x) const -> enable_if_t<!is_floating_point<T>::value, double>
    {
        return abs(x);
    }

    template <typename FT>
    auto operator()(FT fx) const -> enable_if_t<is_floating_point<FT>::value, double>
    {
        return fabs(fx);
    }
};

int main()
{
    typedef boost::variant<int, double, float, complex<double>, short, long long int > VariantNumber;

    vector<VariantNumber> vars;
    vars.push_back(-1);
    vars.push_back(3.14F);
    vars.push_back(-7);
    vars.push_back(static_cast<short>(-78));
    vars.push_back(complex<double>(-1, -1));

    vector<double> modules;

    auto abs_visitor = AbsVisitor{};

    transform(vars.begin(), vars.end(), back_inserter(modules),
              boost::apply_visitor(abs_visitor));

    cout << "modules: ";
    for(const auto& module : modules)
        cout << module << " ";
    cout << endl;
}
