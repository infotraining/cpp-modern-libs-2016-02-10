	#include <iostream>
#include <string>
#include <vector>

#include <boost/any.hpp>
#include <boost/foreach.hpp>

using namespace std;

class A
{
public:
	void some_function() { cout << "A::some_function()" << endl; }
};

class B
{
public:
	void some_function() { cout << "B::some_function()" << endl; }
};

class C
{
public:
	void some_function() { cout << "C::some_function()" << endl; }
};

void print_any(boost::any& a)
{
	if (A* pA = boost::any_cast<A>(&a))
		pA->some_function();
	else if (B* pB = boost::any_cast<B>(&a))
		pB->some_function();
	else if (C* pC = boost::any_cast<C>(&a))
		pC->some_function();
	else
        cout << "Element pominiety." << endl;
}

int main()
{
    // wektor elementow typu any
	vector<boost::any> store_anything;

	// dodawanie do wektora
	store_anything.push_back(A());
	store_anything.push_back(B());
	store_anything.push_back(C());

	store_anything.push_back(string("Tekst..."));
	store_anything.push_back(3);
	store_anything.push_back(make_pair("is_ok", true));

	// iteracja po wektorze
	for_each(store_anything.begin(), store_anything.end(), &print_any);
}
