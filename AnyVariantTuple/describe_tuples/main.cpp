#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <iostream>
#include <string>
#include <map>
#include <memory>
#include <vector>
#include <tuple>

using namespace std;

tuple<int, int, double> calc_stats(const vector<int>& vec)
{
    auto min = *min_element(vec.begin(), vec.end());
    auto max = *max_element(vec.begin(), vec.end());
    auto avg = accumulate(vec.begin(), vec.end(), 0.0) / vec.size();

    return make_tuple(min, max, avg);
}

class Person
{
    int id_;
    string name_;
    uint8_t age_;
public:
    Person(int id, const string& name, uint8_t age)
        : id_{id}, name_{name}, age_{age}
    {}

    bool operator==(const Person& p) const
    {
        return tie(id_, name_, age_) == tie(p.id_, p.name_, p.age_);
    }

    bool operator<(const Person& p) const
    {
        return tied() < p.tied();
    }
private:
    tuple<const int&, const string&, const uint8_t&> tied() const
    {
        return tie(id_, name_, age_);
    }
};

TEST_CASE("tuple")
{
    SECTION("default construction")
    {
        tuple<int, string, double> t;

        REQUIRE(get<0>(t) == 0);
        REQUIRE(get<1>(t) == "");
        REQUIRE(get<2>(t) == 0.0);

        SECTION("in C++14 get can return first item with a given type")
        {
            REQUIRE(get<string>(t) == "");
        }
    }

    SECTION("construction with arguments")
    {
        tuple<int, string, double> t{1, "text", 3.14 };

        REQUIRE(get<0>(t) == 1);
        REQUIRE(get<1>(t) == "text");
        REQUIRE(get<2>(t) == 3.14);
    }

    SECTION("make_tuple creates value tuples")
    {
        tuple<int, string, double> t = make_tuple(1, "text"s, 3.14);

        REQUIRE(get<0>(t) == 1);
        REQUIRE(get<1>(t) == "text");
        REQUIRE(get<2>(t) == 3.14);
    }
}

TEST_CASE("ref tuples")
{
    int x, y;
    double avg;

    SECTION("holds reference to variable")
    {
        tuple<int&, int&, double&> ref_t {x, y, avg };

        get<0>(ref_t) = 13;

        REQUIRE(x == 13);

        SECTION("can be on left hand side of assignment")
        {
            auto t = make_tuple(9, 10, 3.14);

            ref_t = t;

            REQUIRE(x == 9);
            REQUIRE(y == 10);
            REQUIRE(avg == 3.14);
        }
    }

    SECTION("can be created with tie()")
    {
        auto ref_t = tie(x, y, avg);

        get<0>(ref_t) = 13;

        REQUIRE(x == 13);
    }
}


TEST_CASE("calc_stats")
{
    vector<int> data = { 1, 2, 3 };

    SECTION("returns min max avg")
    {
        auto result = calc_stats(data);

        REQUIRE(get<0>(result) == 1);
        REQUIRE(get<1>(result) == 3);
        REQUIRE(get<2>(result) == 2.0);
    }

    SECTION("result can be assigned to ref tuple")
    {
        int min, max;
        double avg;

        tuple<int&, int&, double&> result { min, max, avg };

        result = calc_stats(data);

        REQUIRE(min == 1);
        REQUIRE(max == 3);
        REQUIRE(avg == 2.0);
    }

    SECTION("result can be assigned to tied vars")
    {
        int min, max;
        double avg;

        tie(min, max, avg) = calc_stats(data);

        REQUIRE(min == 1);
        REQUIRE(max == 3);
        REQUIRE(avg == 2.0);
    }

    SECTION("any value from result can be ignored ")
    {
        int min, max;

        tie(min, max, ignore) = calc_stats(data);

        REQUIRE(min == 1);
        REQUIRE(max == 3);
    }
}

TEST_CASE("tie with std::pair")
{
    SECTION("insert to map in c++98")
    {
        map<string, int> dict { {"key1", 1 } } ;

        pair<map<string, int>::iterator, bool> result = dict.insert(make_pair("key1", 1));

        if (result.second)
        {
            cout << "insert was made: " << result.first->first
                 << " - " << result.first->second << endl;
        }
        else
        {
            cout << "item " << result.first->first
                 << " - " << result.first->second << " was in map" << endl;
        }
    }

    SECTION("insert to map in C++11")
    {
        map<string, int> dict { {"key1", 1 } } ;

        bool was_inserted;
        decltype(dict.begin()) where;

        tie(where, was_inserted) = dict.insert(make_pair("key1", 1));

        if (was_inserted)
        {
            cout << "insert was made: " << where->first
                 << " - " << where->second << endl;
        }
        else
        {
            cout << "item " << where->first
                 << " - " << where->second << " was in map" << endl;
        }

    }
}
