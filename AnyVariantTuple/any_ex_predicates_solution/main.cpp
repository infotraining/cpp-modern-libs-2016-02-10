#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <functional>
#include <boost/any.hpp>

using namespace std;

// predykat - funkcja
template <typename T>
bool contains(boost::any& a)
{
	return a.type() == typeid(T);
}

// predykat - obiekt funkcyjny
template <typename T>
class Contains : public unary_function<boost::any, bool>
{
public:
	bool operator()(const boost::any& a) const
	{
		return a.type() == typeid(T);
	}
};

// obiekt funkcyjny
template <typename OutIt, typename Type>
class Extractor
{
	OutIt it_;
public:
	Extractor(OutIt it) : it_(it) {}

	void operator()(boost::any& a)
	{
		Type* t = boost::any_cast<Type>(&a);
		if (t)
			*it_++ = *t;
	}
};

// funkcja pomocnicza
template <typename Type, typename OutIt>
Extractor<OutIt, Type> make_extractor(OutIt it)
{
	return Extractor<OutIt, Type>(it);
}

int main()
{
	vector<boost::any> store_anything;

	store_anything.push_back(1);
	store_anything.push_back(5);
	store_anything.push_back(string("three"));
	store_anything.push_back(3);
	store_anything.push_back(string("four"));
	store_anything.push_back(string("one"));
	store_anything.push_back(string("eight"));
	store_anything.push_back(5);
	store_anything.push_back(4);
	store_anything.push_back(boost::any());
	store_anything.push_back(string("five"));
	store_anything.push_back(string("six"));
	store_anything.push_back(boost::any());


	/* TO DO :
	 * Wykorzystujac algorytmy biblioteki standardowej wykonaj nastapujace czynnosci (napisz odpowiednie
	 * do tego celu predykaty lub obiekty funkcyjne):
	 * 1 - przefiltruj wartości niepuste w kolekcji stored_anything
	 * 2 - zlicz ilosc elementow typu int oraz typu string
	 * 3 - wyekstraktuj z kontenera store_anything do innego kontenera wszystkie elementy typu string
	 */

	// 1
	vector<boost::any> non_empty;
	remove_copy_if(store_anything.begin(), store_anything.end(), back_inserter(non_empty), mem_fun_ref(&boost::any::empty));
	cout << "store_anything.size() = " << store_anything.size() << endl;
	cout << "non_empty.size() = " << non_empty.size() << endl;

	// 2
	int count_int = count_if(store_anything.begin(), store_anything.end(), &contains<int>);
    cout << "stored_anything przechowuje " << count_int << " elementow typu int" << endl;

	int count_string = count_if(store_anything.begin(), store_anything.end(), Contains<string>());
    cout << "stored_anything przechowuje " << count_string << " elementow typu string" << endl;

	// 3
	list<string> string_items;

	for_each(store_anything.begin(),
			 store_anything.end(),
			 make_extractor<string>(back_inserter(string_items)));

	cout << "string_items: ";
	copy(string_items.begin(), string_items.end(),
			ostream_iterator<string>(cout, " "));
	cout << endl;
}
