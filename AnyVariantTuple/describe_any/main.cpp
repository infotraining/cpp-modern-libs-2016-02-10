#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <iostream>
#include <string>
#include <map>
#include <memory>
#include <boost/any.hpp>

using namespace std;

TEST_CASE("boost::any")
{
    SECTION("default constructor creates empty object")
    {
        boost::any obj;

        REQUIRE(obj.empty() == true);
    }

    SECTION("can be initialized with any value")
    {
        boost::any a1 = 1;
        boost::any a2 = "text"s;
        boost::any a3 = make_shared<string>("text");

        SECTION("any value can be assigned to boost::any object")
        {
            a1 = "other text"s;
            a1 = 4.22;

            SECTION("any_cast can return copy of stored value")
            {
                double value = boost::any_cast<double>(a1);

                REQUIRE(value == 4.22);

                SECTION("throws ex when casting to invalid type")
                {
                    REQUIRE_THROWS_AS(boost::any_cast<string>(a1), boost::bad_any_cast);
                }
            }

            SECTION("any_cast can return pointer to stored value")
            {
                boost::any a1 = "text"s;

                string* ptr_text = boost::any_cast<string>(&a1);

                REQUIRE(*ptr_text == "text"s);
            }
        }

        SECTION("any can return type stored inside")
        {
            boost::any a1 = "string"s;

            REQUIRE(a1.type() == boost::typeindex::type_id<string>());

            cout << "a1 stores type: " << a1.type().name() << endl;
        }
    }
}
