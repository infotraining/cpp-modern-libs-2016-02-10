#include <iostream>
#include <string>
#include <tuple>

using namespace std;

tuple<int, string> get_data()
{
	return make_tuple(10, "Zenon Kowalski");
}

int main()
{
	int id;
	string name;

	tie(id, name) = get_data();

	cout << "id = " << id << "; name = " << name << endl;

	// tie dziala rowniez z pair
	tie(id, name) = make_pair(1, "Nikodem Dyzma");

	cout << "id = " << id << "; name = " << name << endl;

	// std::ignore
	string person;

	tie(ignore, person) = get_data();

	cout << "name = " << person << endl;
}

