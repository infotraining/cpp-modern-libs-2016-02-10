#include <string>
#include <iostream>
#include <boost/variant.hpp>

using namespace std;

class A {};

int main()
{
	// zmienna wariantowa
	boost::variant<int, string, double> my_first_variant;

	my_first_variant = 24;
	cout << "my_first_variant = " << my_first_variant << endl;

	my_first_variant = "Tekst...";
	string& txt = boost::get<string>(my_first_variant);
	txt += "ze zmiennej typu variant";
	cout << "my_first_variant = " << my_first_variant << endl;

	my_first_variant = 3.1415;
	cout << "my_first_variant = " << boost::get<double>(my_first_variant) << endl;

	try
	{
		cout << "my_first_variant = " << boost::get<int>(my_first_variant) << endl;
	}
	catch(const boost::bad_get& e)
	{
		cout << "Blad! " << e.what() << endl;
	}

	int* pInt = boost::get<int>(&my_first_variant);
	if (pInt)
		cout << "get<int>() - ok" << endl;
	else
		cout << "get<int>() - nie dziala. W zmiennej przechowywany jest inny typ.";
}
