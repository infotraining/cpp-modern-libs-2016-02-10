#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <functional>
#include <boost/any.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>

using namespace std;

template <typename T>
struct IsType
{
    bool operator()(const boost::any& a) const
    {
        return a.type() == boost::typeindex::type_id<T>();
    }
};

int main()
{
	vector<boost::any> store_anything;

	store_anything.push_back(1);
	store_anything.push_back(5);
	store_anything.push_back(string("three"));
	store_anything.push_back(3);
	store_anything.push_back(string("four"));
	store_anything.push_back(string("one"));
	store_anything.push_back(string("eight"));
	store_anything.push_back(5);
	store_anything.push_back(4);
	store_anything.push_back(boost::any());
	store_anything.push_back(string("five"));
	store_anything.push_back(string("six"));
	store_anything.push_back(boost::any());


	/* TO DO :
     * Wykorzystując algorytmy biblioteki standardowej wykonaj nastapujące czynnosci (napisz odpowiednie
	 * do tego celu predykaty lub obiekty funkcyjne):
     * 1 - przefiltruj wartosci niepuste w kolekcji stored_anything
     */

    auto garbage_begin = remove_if(store_anything.begin(), store_anything.end(),
                                   [](const auto& a) { return a.empty(); });

    assert(store_anything.back().empty() == true);

    store_anything.erase(garbage_begin, store_anything.end());

    assert(all_of(store_anything.begin(), store_anything.end(),
                  [](const auto& a) { return !a.empty();}));


     /* 2 - zlicz ilosc elementow typu int oraz typu string */
//     int int_count = count_if(store_anything.begin(), store_anything.end(),
//                              [](const auto& a) { return a.type() == boost::typeindex::type_id<int>(); });

     int int_count = boost::count_if(store_anything, IsType<int>());
     cout << "int count: " << int_count << endl;

     int string_count = boost::count_if(store_anything, IsType<string>());
     cout << "string count: " << string_count << endl;

     /* 3 - wyekstraktuj z kontenera store_anything do innego kontenera wszystkie elementy typu string */
    vector<string> words;

    boost::transform(store_anything
                        | boost::adaptors::filtered(IsType<string>())
                        | boost::adaptors::reversed,
                     back_inserter(words),
                     [](const auto& a) { return boost::any_cast<string>(a); });

    cout << "words: ";
    for(const auto& item : words)
        cout << item << " ";
    cout << endl;
}
