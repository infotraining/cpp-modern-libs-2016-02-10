#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <iostream>
#include <string>
#include <map>
#include <memory>
#include <vector>
#include <boost/variant.hpp>

using namespace std;

struct PrintVisitor : boost::static_visitor<string>
{
    string operator()(int x) const
    {
        return "int: "s + to_string(x);
    }

    string operator()(double dx) const
    {
        return "double: "s + to_string(dx);
    }

    string operator()(const string& str) const
    {
        return "string: "s + str;
    }

    template <typename T>
    string operator()(const vector<T>& vec) const
    {
        string result = "vector: [ ";
        for(const auto& item : vec)
        {
            result += to_string(item) + " ";
        }
        result += "]";

        return result;
    }
};

TEST_CASE("boost::variant")
{
    SECTION("default constructor creates default value for first type on parameters list")
    {       
        boost::variant<int, string, double> my_variant;

        REQUIRE(boost::get<int>(my_variant) == 0);
    }

    SECTION("can be assigned with any value from list")
    {
        boost::variant<int, string, double> my_variant;

        my_variant = 1;
        my_variant = "Hello";
        my_variant = 5.666;
        my_variant = 5.666F;
        my_variant = static_cast<int>(1u);

        SECTION("get<T> can get value from variant")
        {
            int value = boost::get<int>(my_variant);

            REQUIRE(value == 1);
        }

        SECTION("throws when cast in invalid")
        {
            REQUIRE_THROWS_AS(boost::get<string>(my_variant), boost::bad_get);
        }
    }
}

TEST_CASE("static visitor")
{
    SECTION("can visit variant var")
    {
        boost::variant<int, string, double, vector<int>> my_variant = 6;

        PrintVisitor pv;
        auto value = boost::apply_visitor(pv, my_variant);

        REQUIRE(value == "int: 6"s);

        my_variant = "text";
        value = boost::apply_visitor(pv, my_variant);

        REQUIRE(value == "string: text"s);

        my_variant = vector<int> { 1, 2, 3 };
        value = boost::apply_visitor(pv, my_variant);

        REQUIRE(value == "vector: [ 1 2 3 ]");
    }

    SECTION("deferred visiting")
    {
        using Var = boost::variant<int, string, double, vector<int>>;

        vector<Var> vars = { 1, 2.343, "text"s };

        vector<string> repr;

        PrintVisitor pv;
        transform(vars.begin(), vars.end(), back_inserter(repr),
                  boost::apply_visitor(pv));

        auto expected = { "int: 1"s, "double: 2.343000"s, "string: text"s };

        REQUIRE(equal(repr.begin(), repr.end(), expected.begin()));
    }
}
