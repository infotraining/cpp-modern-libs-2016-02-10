#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <iostream>
#include <stdexcept>
#include <memory>

//#define NDEBUG

#include <boost/polymorphic_cast.hpp>
#include <boost/cast.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

class Gadget
{
    static unsigned int counter_;
public:
    Gadget() : id_(++counter_)
    {
        cout << "Konstruktor Gadget(" << id_ << ")" << endl;
    }

    virtual ~Gadget()
    {
        cout << "Destruktor ~Gadget(" << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }

    virtual void do_stuff() const
    {
        cout << "Gadget::do_stuff()" << endl;
    }

private:
    int id_;
};

unsigned int Gadget::counter_ = 0;

class SuperGadget : public Gadget
{
public:
    SuperGadget()
    {
        cout << "Konstruktor SuperGadget(" << id() << ")" << endl;
    }

    ~SuperGadget()
    {
        cout << "Destruktor ~SuperGadget( " << id() << ")" << endl;
    }

    void do_stuff() const override
    {
        cout << "SuperGadget::do_stuff()" << endl;
    }

	void special_stuff()
	{
		cout << "SuperGadget(id: " << id() << ")::special_stuff()" << endl;
	}
};

TEST_CASE("conversion with polymorhic_cast")
{
    Gadget* g = new Gadget{};

    SECTION("throws when cannot downcast with pointers")
    {
        REQUIRE_THROWS_AS(boost::polymorphic_cast<SuperGadget*>(g), std::bad_cast);
    }

    delete g;
}

TEST_CASE("conversion with polymorphic_downcast")
{
    Gadget* g = new SuperGadget();

    SECTION("throws when bad cast in debug")
    {
        SuperGadget* sg = boost::polymorphic_downcast<SuperGadget*>(g);
        sg->special_stuff();
    }

    delete g;
}

TEST_CASE("conversion with numeric_cast")
{
    int x = 33244;

    SECTION("throws when overflow for integral types")
    {
        short sx{};

        REQUIRE_THROWS_AS(sx = boost::numeric_cast<short>(x), boost::bad_numeric_cast);
    }

    SECTION("throws when sign is a problem")
    {
        x = -24345;

        REQUIRE_THROWS_AS(boost::numeric_cast<unsigned int>(x), boost::bad_numeric_cast);
    }
}

TEST_CASE("conversion with lexical_cast")
{
    SECTION("string->number")
    {
        string str_number = "5";

        SECTION("in C++11 we can use stoi()")
        {
            int number = stoi(str_number);

            REQUIRE(number == 5);
        }

        SECTION("works when cast is safe")
        {
            int number = boost::lexical_cast<int>(str_number);

            REQUIRE(number == 5);
        }

        SECTION("throws when string is ilformed")
        {
            string str = "uiu";

            REQUIRE_THROWS_AS(boost::lexical_cast<int>(str), boost::bad_lexical_cast);
        }

        SECTION("throws when overflow")
        {
            string str = "33333";

            REQUIRE_THROWS_AS(boost::lexical_cast<short>(str), boost::bad_lexical_cast);
        }
    }

    SECTION("number->string")
    {
        int x = 13;

        SECTION("works")
        {
            string str = boost::lexical_cast<string>(x);

            REQUIRE(str == "13");
        }
    }
}
