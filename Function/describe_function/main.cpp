#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <functional>

using namespace std;

void foo(int count)
{
    for(int i = 0; i < count; ++i)
        cout << "foo ";
    cout << endl;
}


class Foo
{
public:
    void operator()(int count)
    {
        for(int i = 0; i < count; ++i)
            cout << "Foo ";
        cout << endl;
    }
};


TEST_CASE("std::function")
{
    function<void(int)> f;

    SECTION("empty f called throws exception")
    {
        REQUIRE_THROWS(f(1));
    }

    SECTION("can store function ptr")
    {
        f = &foo;

        f(2);
    }

    SECTION("can store functor")
    {
        f = Foo();

        f(3);
    }

    SECTION("can store lambda")
    {
        f = [](int count) {
            for(int i = 0; i < count; ++i)
            {
                cout << "lambda ";
            }
            cout << endl;
        };

        f(4);
    }
}
