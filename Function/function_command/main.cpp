#include <iostream>
#include <queue>
#include <functional>
#include <boost/bind.hpp>

using namespace std;

class Worker
{
    typedef std::function<void ()> CommandType;
    queue<CommandType> tasks_;
public:
    void register_task(CommandType cmd)
    {
        tasks_.push(cmd);
    }

    void run()
    {
        while(!tasks_.empty())
        {
            CommandType cmd = tasks_.front();
            cmd();
            tasks_.pop();
        }
    }
};

class Printer
{
public:
    void print(const string& text)
    {
        cout << "Print:" << text << endl;
    }

    void off()
    {
        cout << "Printer.off\n";
    }

    void on()
    {
        cout << "Printer.on\n";
    }
};

int main()
{
    Printer prn;

    Worker worker;

    worker.register_task([&prn] { prn.on();});
    worker.register_task([&prn] { prn.print("Tesks");});
    worker.register_task([&prn] { prn.off(); });

    //...

    worker.run();

    return 0;
}

