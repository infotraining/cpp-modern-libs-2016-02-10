#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <iostream>
#include <string>
#include <map>
#include <memory>

#include <boost/signals2.hpp>

using namespace std;

class Logger
{
public:
    void log(const string& message)
    {
        cout << "Logging: " << message << endl;
    }
};

void save_to_db(const string& table_name, const string& value)
{
    cout << "save to db: " << table_name << " - " << value << endl;
}

void print(double x)
{
    cout << "printing " << x << endl;
}

using namespace boost::signals2;

TEST_CASE("signals2")
{
   boost::signals2::signal<void (double)> sgn;

   SECTION("can connect to function")
   {
       sgn.connect(&print);

       SECTION("can connect to lambda")
       {
           Logger logger;

           sgn.connect([&logger](double value) { logger.log("logging "s + to_string(value));});

           auto connection = sgn.connect([](double value) { save_to_db("Temp", to_string(value));});

           SECTION("calling signal notifies all slots")
           {
               boost::signals2::scoped_connection
                       conn{ sgn.connect([](double) { cout << "from scoped connection" << endl; }) };

               sgn(23.2);
           }

           SECTION("slot can be disconnected")
           {
               connection.disconnect();

               sgn(33.3);
           }
       }
   }
}
