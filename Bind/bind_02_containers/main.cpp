#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>

using namespace std;

class Status
{
	std::string name_;
	bool ok_;
public:
	Status(const std::string& name) : name_(name), ok_(true)
	{
	}
	
	void break_it()
	{
		ok_ = false;
	}
	
	bool is_broken() const
	{
		return ok_;
	}
	
	void report() const
	{
		std::cout << name_ << (ok_ ? " jest w poprawnym stanie" : " jest w niepoprawnym stanie") << std::endl;
	}
};


int main()
{
	std::vector<Status> statuses;
	
	statuses.push_back(Status("status1"));
	statuses.push_back(Status("status2"));
	statuses.push_back(Status("status3"));
	statuses.push_back(Status("status4"));
	statuses.push_back(Status("status5"));
	
	statuses[1].break_it();
	statuses[3].break_it();
	
	// wywolanie funkcji report na rzecz kazdego obiektu w wektorze
	// z wykorzystaniem std::mem_fun_ref
	for_each(statuses.begin(), statuses.end(), std::mem_fun_ref(&Status::report));
	
	std::cout << "-----------------------------------------\n";
	
	// boost::bind
	std::for_each(statuses.begin(), statuses.end(), boost::bind(&Status::report, _1));
    std::for_each(statuses.begin(), statuses.end(), [](const Status& s) { s.report(); });
	
	std::cout << "-----------------------------------------\n";
	
    // wektor wskaźnikow
	std::vector<Status*> p_statuses;
	
	p_statuses.push_back(new Status("status1"));
	p_statuses.push_back(new Status("status2"));
	p_statuses.push_back(new Status("status3"));
	p_statuses.push_back(new Status("status4"));
	p_statuses.push_back(new Status("status5"));
	
	p_statuses[1]->break_it();
	p_statuses[3]->break_it();
	p_statuses[4]->break_it();
	
    // wywołanie funkcji report dla wskaznikow przechowywanych w wektorze
	std::for_each(p_statuses.begin(), p_statuses.end(), std::mem_fun(&Status::report));
	
	std::cout << "-----------------------------------------\n";
	
	std::for_each(p_statuses.begin(), p_statuses.end(), boost::bind(&Status::report, _1));
	
	for(size_t i = 0; i < p_statuses.size(); ++i)
		delete p_statuses[i];

	std::cout << "-----------------------------------------\n";
	
	// wersja ze wskaźnikami shared_ptr
	std::vector<boost::shared_ptr<Status> > s_statuses;
	
	s_statuses.push_back(boost::shared_ptr<Status>(new Status("status1")));
	s_statuses.push_back(boost::shared_ptr<Status>(new Status("status2")));
	s_statuses.push_back(boost::shared_ptr<Status>(new Status("status3")));
	s_statuses.push_back(boost::shared_ptr<Status>(new Status("status4")));
	
	s_statuses[0]->break_it();
	s_statuses[2]->break_it();
	
	for_each(s_statuses.begin(), s_statuses.end(), boost::bind(&Status::report, _1));
}
