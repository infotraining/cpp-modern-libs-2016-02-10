#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <algorithm>
#include <boost/bind.hpp>

#include "PersonalInfo.hpp"


//-----------------------------------------------------------------------
class PersonalInfoAgeComparer
	: public std::binary_function<PersonalInfo, PersonalInfo, bool>
{
public:
	bool operator()(const PersonalInfo& p1, const PersonalInfo& p2) const
	{
		return p1.age() < p2.age();
	}
};

template <typename Container>
void print(const Container& c)
{
	std::copy(c.begin(),
			  c.end(),
			  std::ostream_iterator<typename Container::value_type>(std::cout, "\n"));
}

int main()
{
	std::vector<PersonalInfo> persons;
	persons.push_back(PersonalInfo("Jan", "Kowalski", 23));
	persons.push_back(PersonalInfo("Adam", "Nowak", 45));
	persons.push_back(PersonalInfo("Anna", "Kowalewska", 19));
	persons.push_back(PersonalInfo("Eleonora", "Adamska", 53));


    // sortowanie z uzyciem obiektu funkcyjnego
	std::sort(persons.begin(), persons.end(), PersonalInfoAgeComparer());

	print(persons);

	std::cout << "\n------------------------------------------" << std::endl;

    // sortowanie z uzyciem boost::bind
	std::sort(persons.begin(), persons.end(),
			boost::bind( std::less<int>(),
					boost::bind(&PersonalInfo::age, _1),
					boost::bind(&PersonalInfo::age, _2) ) );

	print(persons);

	std::cout << "\n------------------------------------------" << std::endl;

	// sortowanie w odwrotnej kolejności
	std::sort(persons.begin(), persons.end(), std::not2(PersonalInfoAgeComparer()));

	print(persons);

	std::cout << "\n------------------------------------------" << std::endl;

	// sortowanie z użyciem boost::bind
	std::sort(persons.begin(), persons.end(),
			boost::bind( std::greater<int>(),
					boost::bind(&PersonalInfo::age, _1),
					boost::bind(&PersonalInfo::age, _2) ) );

    std::sort(persons.begin(), persons.end(),
              [](const auto& p1, const auto& p2) { return p1.age() > p2.age();});

	print(persons);

	std::cout << "\n------------------------------------------" << std::endl;

	// wyswietl nazwiska zaczynajace sie na K
	std::remove_copy_if(persons.begin(),
				 persons.end(),
				 std::ostream_iterator<PersonalInfo>(std::cout, "\n"),
				 !boost::bind(std::equal_to<char>(),
						 boost::bind(
								 static_cast<const char& (std::string::*)(size_t) const>(&std::string::at),
								 boost::bind(&PersonalInfo::last_name, _1),
								 0),
						 'K')
	);

//    function<bool(const Person&)> comp
//            = [](const PersonalInfo& p) { return p.last_name()[0] != 'K' };

    auto comp = [](const PersonalInfo& p) { return p.last_name()[0] != 'K' };

    std::remove_copy_if(persons.begin(), persons.end(),
                        std::ostream_iterator<PersonalInfo>(std::cout, "\n"),
                        comp);
}
