#include "bitmap.hpp"
#include <string>
#include <iostream>


using namespace std;

struct Bitmap::BitmapImpl
{
    std::vector<char> image_;

    BitmapImpl(size_t size) : image_(size)
    {
        cout << "BitmapImpl()" << endl;
    }

    ~BitmapImpl()
    {
        cout << "~BitmapImpl()" << endl;
    }
};

Bitmap::Bitmap(const std::string& path) : impl_{new BitmapImpl(path.size())}
{
    copy(path.begin(), path.end(), impl_->image_.begin());
}

Bitmap::~Bitmap() = default;

void Bitmap::draw() const
{
    std::cout << "Drawing: ";

    for(const auto& byte : impl_->image_)
        std::cout << byte;
    std::cout << std::endl;
}
