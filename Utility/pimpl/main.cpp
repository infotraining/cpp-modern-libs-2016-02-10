#include <iostream>
#include <cassert>
#include <boost/utility/addressof.hpp>

#include "bitmap.hpp"

using namespace std;

class Incomplete;

int main(int argc, char *argv[])
{
    Bitmap bmp("drawing.png");

    bmp.draw();

    //Bitmap cpy_bmp = bmp;

    assert(&bmp == 13);
    void* ptr = boost::addressof(bmp);

    cout << "address of bmp: " << ptr << endl;
}
