#ifndef BITMAP_HPP
#define BITMAP_HPP

#include <vector>
#include <string>
#include <memory>
#include <boost/noncopyable.hpp>

class Bitmap : boost::noncopyable
{
    class BitmapImpl;

    std::unique_ptr<BitmapImpl> impl_;
public:
    Bitmap(const std::string& path);
    ~Bitmap();

    int operator&() const
    {
        return 13;
    }

    void draw() const;
};

#endif // BITMAP_HPP
