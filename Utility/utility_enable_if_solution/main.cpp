#include <iostream>
#include <iterator>
#include <list>
#include <type_traits>
#include <algorithm>
#include <cstring>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

using namespace std;

int generic_version_counter = 0;
int optimized_version_counter = 0;

// 1 - napisz generyczny algorytm mcopy kopiujący zakres elementow typu T [first, last) do kontenera rozpoczynającego się od dest
// TODO

template <typename InIt, typename OutIt>
OutIt my_copy(InIt start, InIt end, OutIt dest)
{
    generic_version_counter++;

    for(InIt it = start; it != end; ++it)
        *(dest++) = *it;

    return dest;
}

// 2 - napisz zoptymalizowaną wersję mcopy wykorzystującą memcpy dla tablic T[] gdzie typ T jest typem POD
// TODO

template <typename T>
auto my_copy(const T* start, const T* end, T* dest) -> enable_if_t<is_pod<T>::value, T*>
{
    optimized_version_counter++;

    size_t count = end - start;
    memcpy(dest, start, (count) * sizeof(T));

    return dest + count;
}

TEST_CASE("my_copy")
{
    generic_version_counter = 0;
    optimized_version_counter = 0;

    SECTION("generic version")
    {
        string words[] = { "one", "two", "three", "four" };
        list<string> list_of_words(4);

        my_copy(words, words + 4, list_of_words.begin());

        REQUIRE(equal(begin(words), end(words), list_of_words.begin()));
        REQUIRE(generic_version_counter == 1);
        REQUIRE(optimized_version_counter == 0);
    }

    SECTION("optimized version for POD types and native arrays")
    {
        int numbers[] = { 1, 2, 3, 4, 5 };
        int target[5];

        my_copy(cbegin(numbers), cend(numbers), begin(target));

        REQUIRE(equal(cbegin(numbers), cend(numbers), cbegin(target)));
        REQUIRE(optimized_version_counter == 1);
        REQUIRE(generic_version_counter == 0);
    }
}

