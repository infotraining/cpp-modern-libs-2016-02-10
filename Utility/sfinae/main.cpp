#include <iostream>
#include <type_traits>

using namespace std;

template <bool bool_expression, typename T = void>
struct EnableIf
{
};

template <typename T>
struct EnableIf<true, T>
{
    using type = T;
};

void foo(int x)
{
    cout << "foo(int: " << x << ")" << endl;
}

template <typename T>
auto foo(T object) -> enable_if_t<is_unsigned<T>::value>
{
    cout << "foo(unsigned T: " << object << ")" << endl;
}

template <typename T>
auto foo(T object) -> enable_if_t<is_floating_point<T>::value>
{
    cout << "foo(T: " << object << ")" << endl;
}

int main(int argc, char *argv[])
{
    foo(1);

    foo(1u);

    short sx = 13;
    foo(sx);

    unsigned short usx = 13;
    foo(usx);

    foo(5.3434);

    foo(76.23F);
}
