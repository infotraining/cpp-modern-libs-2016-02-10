#include <iostream>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits.hpp>
#include <c++/typeinfo>
#include <utility>

template <bool bool_expression, typename T = void>
struct EnableIf
{
};

template <typename T>
struct EnableIf<true, T>
{
    using type = T;
};

void some_func(int i)
{
	std::cout << "void some_func(" << i << ")\n";
}


class ExtraClass
{
    int value_;
public:
    explicit ExtraClass(int value) : value_{value}
    {}

    int value() const
    {
        return value_;
    }
};

std::ostream& operator<<(std::ostream& out, const ExtraClass& ec)
{
    out << ec.value();

    return out;
}

template <typename T>
struct HasValueMember : boost::false_type {};

template <>
struct HasValueMember<ExtraClass> : boost::true_type {};


template <typename T>
void some_func(const T& arg, typename EnableIf<HasValueMember<T>::value>::type* ptr = nullptr)
{
    std::cout << "void some_func<T: " << typeid(T).name() << ">(" << arg << ")" << std::endl;
}

template <typename T>
typename boost::enable_if<boost::is_unsigned<T>>::type some_func(const T& arg)
{
    std::cout << "void some_func<unsigned>(" << arg << ")" << std::endl;
}

template <typename T>
typename boost::enable_if<boost::is_floating_point<T>>::type some_func(const T& arg)
{
    std::cout << "void some_func<floatinh point>(" << arg << ")" << std::endl;
}


int main()
{
	int i = 12;
	short s = 12;
	unsigned int ui = 12;
	unsigned short us = 12;
	ExtraClass ec{12};
    double dx = 9.22;

	some_func(i);
	some_func(s);
	some_func(ui);
	some_func(us);
	some_func(ec);
    some_func(dx);
}
