#include <iostream>
#include <type_traits>
#include <memory>

using namespace std;

template <typename T>
struct IsVoid : public false_type
{};

template <>
struct IsVoid<void> : public true_type
{
};

// ---------------------------------------

template <typename T>
struct IsPointer : public false_type
{
};

template <typename T>
struct IsPointer<T*> : public true_type
{
};

// ----------------------------------------

class ExtraType
{
public:
    int value()
    {

    }
};

template <typename T>
struct HasValueMember : public false_type
{};

template <>
struct HasValueMember<ExtraType> : public true_type
{};

template <typename T>
void use(T obj)
{
    static_assert(HasValueMember<T>::value, "T must have value() member or HasValueMember trait must defined");

    obj.value();
}

class OtherType
{
public:
   string value()
   {
       return "value"s;
   }
};

template <>
struct HasValueMember<OtherType> : public true_type
{};

// ---------------------------------------------------------

template <typename T>
struct IsAutoPtr : false_type
{};

template <typename T>
struct IsAutoPtr<std::auto_ptr<T>> : true_type
{};

template <typename T>
void use_pointer(T ptr)
{
    static_assert(IsAutoPtr<T>::value == false, "T cannot be auto_ptr");

    cout << "Using: " << *ptr << endl;
}


int main(int argc, char *argv[])
{
    static_assert(IsVoid<int>::value == false, "Error");
    static_assert(IsVoid<void>::value == true, "Error");

    int* ptr;
    static_assert(is_pointer<decltype(ptr)>::value, "Error");

    OtherType obj;

    use(obj);

    int x = 10;
    use_pointer(&x);

    shared_ptr<int> sp = make_shared<int>(12);
    use_pointer(sp);

//    auto_ptr<int> ap(new int(13));
//    use_pointer(ap);

//    cout << *ap << endl;
}
