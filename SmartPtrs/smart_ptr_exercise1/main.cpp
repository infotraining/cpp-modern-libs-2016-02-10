#include <memory>
#include <iostream>
#include <vector>
#include <exception>
#include <stdexcept>

using namespace std;

class Gadget
{
public:
	// konstruktor
    Gadget(int value = 0)
        : value_ {value}
	{
        std::cout << "Konstruktor Gadget(" << value_ << ")\n";
	}
	
	// destruktor
    ~Gadget()
	{
        std::cout << "Destruktor ~Gadget(" << value_ << ")\n";
	}
	
    int value() const
	{
		return value_;
	}

    void set_value(int value)
	{
        value_ = value;
	}

    void unsafe()
    {
        throw std::runtime_error("ERROR");
    }

private:
	int value_;
};

namespace LegacyCode
{
    Gadget* create_many_gadgets(unsigned int size)
    {
        Gadget* xarray = new Gadget[size];

        for(unsigned int i = 0; i < size; ++i)
            xarray[i].set_value(i);

        return xarray;
    }
}

void reset_value(Gadget& g, int n)
{
    // some logic

    g.set_value(n * n);
    cout << "New value for Gadget: " << g.value() << endl;
}

//unique_ptr<Gadget> create_gadget(int arg)
//{
//    return make_unique<Gadget>(arg);
//}

void unsafe1()  // TODO: poprawa z wykorzystaniem smart_ptr
{
    auto ptr_gdgt = make_unique<Gadget>(4);

	/* kod korzystajacy z ptrX */

    reset_value(*ptr_gdgt, 5);

    ptr_gdgt->unsafe();
}

void unsafe2()
{
    int size = 10;

    unique_ptr<Gadget[]> buffer{LegacyCode::create_many_gadgets(size)};

    /* kod korzystający z buffer */

    for(int i = 0; i < size; ++i)
        buffer[0].unsafe();
}

using GadgetPtr = unique_ptr<Gadget>;

void unsafe3()
{
    vector<GadgetPtr> my_gadgets;

    my_gadgets.push_back(make_unique<Gadget>(87));
    my_gadgets.push_back(make_unique<Gadget>(12));
    my_gadgets.push_back(make_unique<Gadget>(98));

    int value_generator = 0;
    for(const auto& gadget : my_gadgets)
    {
        cout << "Gadget's old value: " << gadget->value() << endl;
        reset_value(*gadget, ++value_generator);
    }

    my_gadgets[0].reset();

    my_gadgets[1]->unsafe();   
}

int main() try
{
    try
    {
        unsafe1();
        unsafe2();
        unsafe3();
    }
    catch(const exception& e)
    {
        cout << "Exception caught: " << e.what() << endl;
    }
}
catch(const exception& e)
{
    cout << "Exception caught: " << e.what() << endl;
}
