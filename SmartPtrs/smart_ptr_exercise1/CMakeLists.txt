get_filename_component(PROJECT_NAME_STR ${CMAKE_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${PROJECT_NAME_STR})

cmake_minimum_required(VERSION 2.8)
project(${PROJECT_NAME_STR})

#----------------------------------------
# set compiler
#----------------------------------------
if(CMAKE_COMPILER_IS_GNUCXX)
        add_definitions(-Wall -std=c++14)
endif()

if (MSVC)
	add_definitions(-D_SCL_SECURE_NO_WARNINGS) 
endif()


find_package(Boost 1.50.0)
if(Boost_FOUND)
	include_directories(${Boost_INCLUDE_DIRS})
endif()

#----------------------------------------
# Application
#----------------------------------------
aux_source_directory(. SRC_LIST)

# Headers
file(GLOB HEADERS_LIST "*.h" "*.hpp")
add_executable(${PROJECT_NAME} ${SRC_LIST} ${HEADERS_LIST})
