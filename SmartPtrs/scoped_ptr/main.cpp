#include <iostream>
#include <stdexcept>
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>
#include <cstdlib>

using namespace std;

class Gadget
{
    static unsigned int counter_;
public:
    Gadget() : id_(++counter_)
    {
        cout << "Konstruktor Gadget(" << id_ << ")" << endl;
    }

    virtual ~Gadget()
    {
        cout << "Destruktor ~Gadget(" << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }

    virtual void do_stuff() const
    {
        cout << "Gadget::do_stuff()" << endl;
    }

private:
    int id_;
};

unsigned int Gadget::counter_ = 0;

int main()
{
	try
	{
		boost::scoped_ptr<Gadget> my_gadget(new Gadget());
		my_gadget->do_stuff();
		my_gadget.reset();

		boost::scoped_ptr<Gadget> your_gadget(new Gadget());
		
		your_gadget->do_stuff();
		
		throw std::runtime_error("Błąd");
		
		your_gadget->do_stuff();
	}
	catch (...)
	{
		cout << "Obsługa wyjątku." << endl;
	}

	cout << "--------------------------------------\n";

	try
	{
		// prezenatacja dzialania scoped_array
		boost::scoped_array<Gadget> gadgets(new Gadget[10]);

		for(size_t i = 0; i < 10; ++i)
			gadgets[i].do_stuff();

		throw std::runtime_error("Runtime Exception");
	}
	catch (...)
	{
		cout << "Obsluga wyjatku" << endl;
	}
}

