#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <algorithm>
#include <vector>
#include <memory>
#include <boost/scoped_ptr.hpp>

using namespace std;

long long int id_generator()
{
    static long long int id = 0;
    return ++id;
}

class Gadget
{
    long long int id_ {-1};
    std::string name_ {"unknown"};
public:
    Gadget() = default;

    Gadget(const std::string& name)
        : id_{id_generator()}, name_{name}
    {}

    Gadget(const Gadget&) = default;
    Gadget& operator=(const Gadget&) = default;

    Gadget(Gadget&&) = default;
    Gadget& operator=(Gadget&&) = default;

    virtual ~Gadget() = default;

    std::string name() const
    {
        return name_;
    }

    long long int id() const
    {
        return id_;
    }
};

class MockGadget : public Gadget
{
    std::function<void()> callable_;
public:
    MockGadget(std::function<void()> callable) : callable_{callable}
    {}

    ~MockGadget()
    {
        callable_();
    }
};


TEST_CASE("scoped_ptr")
{
    SECTION("default constructor creates empty pointer")
    {
        boost::scoped_ptr<Gadget> ptr_gadget;

        REQUIRE(ptr_gadget.get() == nullptr);
    }

    SECTION("implements raii")
    {
        bool destroyed = false;

        {
            boost::scoped_ptr<Gadget> ptr_gadget{
                new MockGadget{ [&destroyed] { destroyed = true;} } };

            SECTION("has pointer interface")
            {
                REQUIRE(ptr_gadget->name() == "unknown");
                REQUIRE((*ptr_gadget).id() == -1);
            }

            SECTION("get() returns pointer to resource")
            {
                Gadget* raw_ptr_gadget = ptr_gadget.get();

                REQUIRE(raw_ptr_gadget != nullptr);
                REQUIRE(raw_ptr_gadget->id() == -1);
            }
        }

        REQUIRE(destroyed == true);
    }

    SECTION("reset() can destroy an object on demand")
    {
        bool destroyed = false;

        {
            boost::scoped_ptr<Gadget> ptr_gadget{
                new MockGadget{ [&destroyed] { destroyed = true;} } };

            ptr_gadget.reset();

            REQUIRE(destroyed == true);
        }
    }
}

namespace LegacyCode
{
    auto_ptr<Gadget> gadget_factory(const std::string& name)
    {
        return auto_ptr<Gadget>{new Gadget{name}};
    }

    void use_gadget(auto_ptr<Gadget> g)
    {
        cout << "Using: " << g->name() << endl;
    }
}

unique_ptr<Gadget> gadget_factory(const std::string& name)
{
    return make_unique<Gadget>(name);
}

void use_gadget(unique_ptr<Gadget> g)
{
    cout << "Using: " << g->name() << endl;
}

TEST_CASE("auto_ptr")
{
    SECTION("is copyable")
    {
        auto_ptr<Gadget> g1{new Gadget{"ipad"}};

        auto_ptr<Gadget> g2 = g1;

        REQUIRE(g1.get() == nullptr);
        REQUIRE(g2->name() == "ipad");
    }

    SECTION("can be returned from source functions")
    {
        auto_ptr<Gadget> g = LegacyCode::gadget_factory("ipad");

        REQUIRE(g->name() == "ipad");
    }

    SECTION("can be used as argument in sink functions")
    {
        LegacyCode::use_gadget(LegacyCode::gadget_factory("ipad"));

        SECTION("and it can cause problems")
        {
            auto_ptr<Gadget> g = LegacyCode::gadget_factory("ipad");

            LegacyCode::use_gadget(g);

            //g->name();  // UB
        }
    }
}


TEST_CASE("default move operations")
{
    Gadget g{"ipad"};
    auto id = g.id();

    SECTION("will forward move for every member")
    {
        Gadget other = move(g);

        REQUIRE(other.id() == id);
        REQUIRE(other.name() == "ipad");

        REQUIRE(g.id() == id);
        REQUIRE(g.name() == "");
    }
}

TEST_CASE("uniform init syntax")
{
    SECTION("can call constructors")
    {
        Gadget g{"mp3 player"};

        REQUIRE(g.name() == "mp3 player");
    }

    SECTION("vector intialized with {} uses vector(std::initializer_list<T>)")
    {
        vector<int> vec{10, 2};

        REQUIRE(vec.size() == 2);
        REQUIRE(vec[0] == 10);
        REQUIRE(vec[1] == 2);
    }

    SECTION("vector initialized with() uses vector(size_t, T)")
    {
        vector<int> vec(10, 2);

        REQUIRE(vec.size() == 10);
        REQUIRE(all_of(vec.begin(), vec.end(), [](const auto& item) { return item == 2;}));
    }
}
