#include <iostream>
#include <atomic>
#include <boost/intrusive_ptr.hpp>
#include <boost/noncopyable.hpp>

// definicja funkcji intrusive_ptr_add_ref i intrusive_ptr_release
template <typename T>
void intrusive_ptr_add_ref(T* t)
{
	t->add_ref();
}

template <typename T>
void intrusive_ptr_release(T* t)
{
	t->release();
}

// klasa licznika
class ReferenceCounter : boost::noncopyable
{
	std::atomic<int> ref_count_;
public:
	ReferenceCounter() : ref_count_{0} {}
	
	virtual ~ReferenceCounter() {}
	
	void add_ref()
	{
        std::atomic_fetch_add_explicit(&ref_count_, 1, std::memory_order_relaxed);
	}
	
	void release()
	{
		if (std::atomic_fetch_sub_explicit(&ref_count_, 1, std::memory_order_release) == 1)
        {
            std::atomic_thread_fence(std::memory_order_acquire);

            delete this;
        }
	}
};

// zarzadzana klasa
class Gadget : public ReferenceCounter
{
public:
	Gadget()
	{
		std::cout << "Gadget::Gadget()\n";
	}

    Gadget(const Gadget&) = delete;
    Gadget& operator=(const Gadget&) = delete;
	
	~Gadget()
	{
		std::cout << "Gadget:~Gadget()\n";
	}
};

int main()
{
	std::cout << "Przed wejsciem do zasiegu" << std::endl;
	{
		boost::intrusive_ptr<Gadget> p1(new Gadget());
		{
			boost::intrusive_ptr<Gadget> p2 = p1;
		}
	}
	std::cout << "Po wyjsciu z zasiegu" << std::endl;
}
