#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <algorithm>
#include <vector>
#include <memory>

using namespace std;

long long int id_generator()
{
    static long long int id = 0;
    return ++id;
}

class Gadget
{
    long long int id_ {-1};
    std::string name_ {"unknown"};
public:
    Gadget() = default;

    Gadget(const std::string& name)
        : id_{id_generator()}, name_{name}
    {}

    Gadget(const Gadget&) = default;
    Gadget& operator=(const Gadget&) = default;

    Gadget(Gadget&&) = default;
    Gadget& operator=(Gadget&&) = default;

    virtual ~Gadget() = default;

    std::string name() const
    {
        return name_;
    }

    long long int id() const
    {
        return id_;
    }

    virtual void run()
    {
        cout << "Gadget: id = " << id() << "; name = " << name()
             << ") is running" << endl;
    }
};

class SuperGadget : public Gadget
{
public:
    using Gadget::Gadget;

    virtual void run() override
    {
        cout << "SuperGadget: id = " << id() << "; name = " << name()
             << ") is running" << endl;
    }

    void super_gadget_only()
    {

    }
};

class MockGadget : public Gadget
{
    std::function<void()> callable_;
public:
    MockGadget(std::function<void()> callable) : callable_{callable}
    {}

    ~MockGadget()
    {
        callable_();
    }
};

class CustomGadgetDeallocator
{
public:
    void operator()(Gadget* g)
    {
        if (g)
        {
            cout << "CustomGadgetDeallocator works for: Gadget("
                 << g->id() << " - " << g->name() << ")" << endl;
        }

        delete g;
    }
};

TEST_CASE("shared_ptr")
{
    SECTION("can be initialized wit RAII constructor")
    {
        shared_ptr<Gadget> g1{ new Gadget{"ipad"} };

        REQUIRE(g1->name() == "ipad");
        REQUIRE(g1.use_count() == 1);
        REQUIRE(g1.unique() == true);

        SECTION("is copyable")
        {
            shared_ptr<Gadget> g2 = g1;

            REQUIRE(g2->name() == "ipad");
            REQUIRE(g2.use_count() == 2);
        }
    }

    SECTION("should be initialized with make_shared factory")
    {
        auto g1 = make_shared<Gadget>("ipad");

        REQUIRE(g1->name() == "ipad");
        REQUIRE(g1.use_count() == 1);
    }

    SECTION("can use custom deallocator")
    {
        shared_ptr<Gadget> g1{new Gadget{"ipad"}, CustomGadgetDeallocator()};
    }

    SECTION("calls deallocator with nullptr inside")
    {
        bool dealloc_was_called = false;

        {
            shared_ptr<Gadget> g{nullptr, [&dealloc_was_called](Gadget*) { dealloc_was_called = true; }};
        }

        REQUIRE(dealloc_was_called == true);
    }
}

TEST_CASE("casting shared+ptrs")
{
    shared_ptr<Gadget> g = make_shared<SuperGadget>("ipad2");

    SECTION("static_cast")
    {
        shared_ptr<SuperGadget> sg = static_pointer_cast<SuperGadget>(g);

        sg->super_gadget_only();
    }

    SECTION("dynamic_cast")
    {
        shared_ptr<SuperGadget> sg = dynamic_pointer_cast<SuperGadget>(g);

        sg->super_gadget_only();
    }
}

TEST_CASE("weak_ptr")
{
    SECTION("can be created from shared_ptr")
    {
        shared_ptr<Gadget> g = make_shared<Gadget>("ipad");

        weak_ptr<Gadget> wg = g;

        REQUIRE(g.use_count() == 1);
        REQUIRE(wg.use_count() == 1);
        REQUIRE(wg.expired() == false);

        SECTION("can be converted to shared_ptr")
        {
            SECTION("with lock()")
            {
                shared_ptr<Gadget> local_g = wg.lock();

                if (local_g)
                {
                    local_g->run();
                    REQUIRE(g.use_count() == 2);
                }
            }

            SECTION("with shared_ptr constructor")
            {
                shared_ptr<Gadget> local_g{wg};

                local_g->run();
                REQUIRE(g.use_count() == 2);
            }
        }

        SECTION("when wg is expired")
        {
            SECTION("lock() returns empty shared_ptr ")
            {
                g.reset();

                auto local_g = wg.lock();

                REQUIRE(local_g == nullptr);
            }

            SECTION("shared_ptr constructor throws exception")
            {
                g.reset();

                REQUIRE_THROWS(shared_ptr<Gadget>{wg});
            }
        }
    }
}





