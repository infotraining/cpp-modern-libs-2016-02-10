#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <cassert>
#include <memory>

const char* get_line()
{
	static unsigned int count = 0;

	if (++count == 13)
		throw std::runtime_error("Blad!!!");

	return "Hello RAII\n";
}

void save_to_file(const char* file_name)
{
	FILE* file = fopen(file_name, "w");

	if (!file)
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for(size_t i = 0; i < 100; ++i)
		fprintf(file, get_line());

	fclose(file);
}

class FileGuard
{
    FILE* file_;
public:
    explicit FileGuard(FILE* file) : file_{file}
    {}

    FileGuard(const FileGuard&) = delete;
    FileGuard& operator=(const FileGuard&) = delete;

    FileGuard(FileGuard&& source) noexcept : file_{source.file_}
    {
        source.file_ = nullptr;
    }

    FileGuard& operator =(FileGuard&& source) noexcept
    {
        if (this != &source)
        {
            file_ = source.file_;
            source.file_ = nullptr;
        }

        return *this;
    }

    FILE* get() const
    {
        return file_;
    }

    explicit operator bool() const
    {
        return file_ != nullptr;
    }

    ~FileGuard()
    {
        if (file_)
            fclose(file_);
    }
};


// TO DO: RAII
void save_to_file_with_raii(const char* file_name)
{
    FileGuard file {fopen(file_name, "w")};
    FileGuard file2 = std::move(file);

    assert(file.get() == nullptr);

    if (!file2)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file2.get(), get_line());
}

void save_to_file_with_unique_ptr(const char* file_name)
{
    using FileCloser = int(*)(FILE*);
    std::unique_ptr<FILE, FileCloser> file{fopen(file_name, "w"), &fclose};

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

void save_to_file_with_shared_ptr(const char* file_name)
{
    std::shared_ptr<FILE> file{fopen(file_name, "w"),
                               [](FILE* f) { if (f) fclose(f); }};

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

int main()
try
{
    //save_to_file("text.txt");
    //save_to_file_with_raii("text.txt");
    //save_to_file_with_unique_ptr("text.txt");
    save_to_file_with_shared_ptr("text.txt");
}
catch(const std::exception& e)
{
	std::cout << e.what() << std::endl;

	std::cout << "Press a key..." << std::endl;
    std::string temp;
    std::getline(std::cin, temp);
}
