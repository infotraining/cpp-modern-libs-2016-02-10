#include <memory>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <algorithm>

using namespace std;

class Object
{
public:
	Object(int id = 0) : id_(id) 
	{
		cout << "Constructor of an Object " << id_ << "\n";
	}
	
	~Object()
	{
		cout << "Destructor of an object Object " << id_ << "\n";
	}
	
	void run() const
	{
		cout << "Object(" << id_ << ")::run()\n";
	}
private:
	int id_;
};

int main()
{
	shared_ptr<Object> external;
	try
	{
		vector<shared_ptr<Object>> vec;
		
		vec.push_back(make_shared<Object>(1));
		vec.push_back(make_shared<Object>(2));
		vec.emplace_back(new Object(3));
		vec.push_back(make_shared<Object>(4));
		
		vec[0]->run();

		external = vec[0];
		
		vec.at(100)->run();
	}
	catch(const exception& e)
	{
		cout << "Exception: " << e.what() << endl;
	}
}
