#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <algorithm>
#include <vector>
#include <memory>
#include <boost/scoped_ptr.hpp>

using namespace std;

long long int id_generator()
{
    static long long int id = 0;
    return ++id;
}

class Gadget
{
    long long int id_ {-1};
    std::string name_ {"unknown"};
public:
    Gadget() = default;

    Gadget(const std::string& name)
        : id_{id_generator()}, name_{name}
    {}

    Gadget(const Gadget&) = default;
    Gadget& operator=(const Gadget&) = default;

    Gadget(Gadget&&) = default;
    Gadget& operator=(Gadget&&) = default;

    virtual ~Gadget() = default;

    std::string name() const
    {
        return name_;
    }

    long long int id() const
    {
        return id_;
    }
};

class MockGadget : public Gadget
{
    std::function<void()> callable_;
public:
    MockGadget(std::function<void()> callable) : callable_{callable}
    {}

    ~MockGadget()
    {
        callable_();
    }
};

unique_ptr<Gadget> gadget_factory(const std::string& name)
{
    return make_unique<Gadget>(name);
}

void use_gadget(unique_ptr<Gadget> g)
{
    cout << "Using: " << g->name() << endl;
}

class CustomGadgetDeallocator
{
public:
    void operator()(Gadget* g)
    {
        cout << "CustomGadgetDeallocator works for: Gadget("
             << g->id() << " - " << g->name() << ")" << endl;

        delete g;
    }
};

void deallocate_gadget(Gadget* g)
{
    cout << "deallocate_gadget works for: Gadget("
         << g->id() << " - " << g->name() << ")" << endl;

    delete g;
}

TEST_CASE("unique_ptr")
{
    SECTION("default constructor creates an empty pointer")
    {
        unique_ptr<Gadget> ptr;

        REQUIRE(ptr.get() == nullptr);
    }

    SECTION("can be initialized with pointer to dynamic object")
    {
        unique_ptr<Gadget> g{new Gadget{"ipad"}};
    }

    SECTION("has factory make_unique() since C++14")
    {
        unique_ptr<Gadget> g = make_unique<Gadget>("ipad");

        REQUIRE(g->name() == "ipad");
    }

    SECTION("when assigned it deallocates previous object")
    {
        bool destroyed = false;

        unique_ptr<Gadget> g = make_unique<MockGadget>([&destroyed] { destroyed = true;});

        SECTION("with assignment of r-value")
        {
            g = make_unique<Gadget>("ipad");

            REQUIRE(destroyed == true);
        }

        SECTION("with moved l-value")
        {
            unique_ptr<Gadget> g2 = make_unique<Gadget>("ipad");

            g = move(g2);

            REQUIRE(destroyed == true);
            REQUIRE(g2.get() == nullptr);
        }
    }

    SECTION("is no copyable")
    {
        auto g = make_unique<Gadget>("ipad");

        //unique_ptr<Gadget> g2 = g;  // compilation error
    }

    SECTION("can be returned from source functions")
    {
        unique_ptr<Gadget> g = gadget_factory("ipad");

        REQUIRE(g->name() == "ipad");
    }

    SECTION("can be used as argument in sink functions")
    {
        use_gadget(gadget_factory("ipad"));

        SECTION("and it can be moved from l-values")
        {
            unique_ptr<Gadget> g = gadget_factory("ipad");

            use_gadget(move(g));

            REQUIRE(g.get() == nullptr);
        }
    }

    SECTION("can manage dynamic arrays")
    {
        unique_ptr<Gadget[]> gadgets = make_unique<Gadget[]>(10);

        REQUIRE(gadgets[0].name() == "unknown");
    }

    SECTION("works with custom deallocators")
    {

        SECTION("as functor")
        {
            unique_ptr<Gadget, CustomGadgetDeallocator> g{new Gadget{"ipad"}};
        }

        SECTION("as function pointer")
        {
            unique_ptr<Gadget, void(*)(Gadget*)> g(new Gadget{"ipod"}, &deallocate_gadget);
        }

        SECTION("as lambda")
        {
            auto lambda_dealloc = [](Gadget* g) {
                cout << "lambda_dealloc works for: Gadget("
                     << g->id() << " - " << g->name() << ")" << endl;

                delete g;
            };

            unique_ptr<Gadget, decltype(lambda_dealloc)> g(new Gadget{"mp3 player"}, lambda_dealloc);
        }
    }

    SECTION("can be stored in std containers")
    {
        vector<unique_ptr<Gadget>> gadgets;

        gadgets.push_back(make_unique<Gadget>("ipad"));

        auto g = make_unique<Gadget>("ipod");
        gadgets.push_back(move(g));

        REQUIRE(gadgets[0]->name() == "ipad");
        REQUIRE(gadgets[1]->name() == "ipod");

        auto other_gadget = move(gadgets[1]);
        REQUIRE(other_gadget->name() == "ipod");
        REQUIRE(gadgets[1].get() == nullptr);
    }
}

class ClosureClassForLambda_234234
{
public:
    void operator()(Gadget* g) const
    {
        cout << "lambda_dealloc works for: Gadget("
             << g->id() << " - " << g->name() << ")" << endl;

        delete g;
    }
};

class ClosureClassLambda_3462456245
{
    bool& destroyed;
public:
    ClosureClassLambda_3462456245(bool& destroyed) : destroyed{destroyed}
    {}

    void operator()() const
    {
        destroyed = true;
    }
};

