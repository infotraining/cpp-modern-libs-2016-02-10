#include <iostream>
#include <stdexcept>
#include <memory>

using namespace std;

class Gadget
{
    static unsigned int counter_;
public:
    Gadget() : id_(++counter_)
    {
        cout << "Konstruktor Gadget(" << id_ << ")" << endl;
    }

    virtual ~Gadget()
    {
        cout << "Destruktor ~Gadget(" << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }

    virtual void do_stuff() const
    {
        cout << "Gadget::do_stuff()" << endl;
    }

private:
    int id_;
};

class SuperGadget : public Gadget
{
public:
    SuperGadget()
    {
        cout << "Konstruktor SuperGadget(" << id() << ")" << endl;
    }

    ~SuperGadget()
    {
        cout << "Destruktor ~SuperGadget( " << id() << ")" << endl;
    }

    void do_stuff() const override
    {
        cout << "SuperGadget::do_stuff()" << endl;
    }

	void special_stuff()
	{
		cout << "SuperGadget(id: " << id() << ")::special_stuff()" << endl;
	}
};

enum class GadgetType
{
    normal, super
};

std::unique_ptr<Gadget> create_gadget(GadgetType type)
{
    std::unique_ptr<Gadget> gadget; // intialized with nullptr

    if (type == GadgetType::normal)
        gadget.reset(new Gadget());
    else if (type == GadgetType::super)
        gadget.reset(new SuperGadget());
    else
        throw std::runtime_error("Bad gadget type");

    return gadget;
}

unsigned int Gadget::counter_ = 0;

int main()
{
    // block of code
    {
        std::unique_ptr<Gadget> my_gadget = std::make_unique<Gadget>();
        std::unique_ptr<Gadget> mm = std::make_unique<Gadget>();
    }

    cout << "\n\n";

    auto my_gadget = create_gadget(GadgetType::super);

    my_gadget->do_stuff();

    std::unique_ptr<Gadget> your_gadget = move(my_gadget);

    your_gadget->do_stuff();

	SuperGadget* ptr_super = static_cast<SuperGadget*>(your_gadget.get());

	ptr_super->special_stuff();    
    ptr_super->do_stuff();
}
